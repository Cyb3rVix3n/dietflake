<!---
SPDX-FileCopyrightText: 2023 Triss <Cyb3rVix3n@runbox.com>

SPDX-License-Identifier: MIT
--->

# Supported Devices

| Device            | Status                     | File System |
|-------------------|----------------------------|-------------|
| Pine64 A64        | tested with DietPi v8.17.2 | ext4        |
| Raspberry Pi 3 B+ | tested with DietPi v8.17.2 | FAT         |
