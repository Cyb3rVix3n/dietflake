<!---
SPDX-FileCopyrightText: 2023 Triss <Cyb3rVix3n@runbox.com>

SPDX-License-Identifier: MIT
--->

# Installation

This page gives instructions on how to simply install a Snowflake proxy on a SoC, using the DietPi operating system and the set of DietFlake scripts.

## Before You Start

Check whether your own internet connection is censored. You can use for example the ranking at [Freedomhouse.](https://freedomhouse.org/countries/freedom-net/scores)

### Needed Hardware

* A supported SoC including a fitting power supply (see [List of Supported Devices](Supported_Devices.md))
* A fitting SD card for your SoC (min. 4 GB)
* A network cable and a free port on your router
* A computer to write the initial image to the SD card

### Needed Software

* The DietPi image for your supported SoC
* The dietpi.txt file from this repository
* An etcher software to write the initial image to the SD card (depending on your computer and operating system)

## Steps for Installation

**WARNING! Risk of data loss! With the etcher software you can easily wipe your hard drive. Tripple check that you picked the correct device to etch.**
1. Etch DietPi to your SD card using the etcher software
2. Unplug and re-plug the SD card. You should see a FAT-formated drive pop up (if not, have a look [here.](Install_on_ext4_en.md))
3. Replace dietpi.txt in the root folder of that drive with the file you downloaded from this repository
4. Open dietpi.txt on the SD card, go to line 131 and replace the root passwort with a password of your choice. You can also just search for “TODO” to find the correct line.
5. Plug the SD card into your SoC and connect it to your router.
6. Power up your SoC. The system will install and configure all by itself. 

Congratulations! You just set up a Snowflake proxy for the Tor network to help people circumvent censorship.
