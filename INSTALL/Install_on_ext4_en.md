<!---
SPDX-FileCopyrightText: 2023 Triss <Cyb3rVix3n@runbox.com>

SPDX-License-Identifier: MIT
--->

# Installation without a FAT file system
In case you don’t see a FAT-formatted partition after re-plugging your SD-card, there is only an ext4-formatted partition on the card which can’t be read by a Windows system.
But that doesn’t mean you have to stop here. All you need is a system that’s capable of reading and writing to ext4-partitions (e.g. linux).

Mount the ext4-partition and replace the dietpi.txt in the boot folder with the one from this repository (you might need root privileges for this).

Afterwards you can simply return to [the installation instructions](Installation_en.md) and continue at Step 4 (you might still need root privileges to edit dietpi.txt).
