#!/bin/sh
# SPDX-FileCopyrightText: 2023 Triss <Cyb3rVix3n@runbox.com>
# 
# SPDX-License-Identifier: GPL-3.0-or-later

# Script to install Tor SnowFlake to your freshly installed DietPi
#
# install docker-compose via apt
apt install -y docker-compose 
# get docker-compose file
curl https://gitlab.torproject.org/tpo/anti-censorship/docker-snowflake-proxy/raw/main/docker-compose.yml > docker-compose.yml
# switch version number to 3.3 for docker version in debian stable
sed s/version:\ \"\..\.\"/version:\ \"3.3\"/g docker-compose.yml > docker-compose.yml
# compose snowflake-proxy
docker-compose up -d snowflake-proxy
# start snowflake-proxy
docker run thetorproject/snowflake-proxy > /home/dietpi/snowflake.log 2>&1 &

# create temporary entry to start snowflake-proxy at startup and write log to /home/dietpi
echo "@reboot /usr/bin/docker run thetorproject/snowflake-proxy >> /home/dietpi/snowflake.log 2>&1 &" > crontab.tmp
# create temporary entry to update snowflake-proxy every day
echo "0 0 * * * /usr/bin/docker update thetorproject/snowflake-proxy" >> crontab.tmp
# create temporary entry to update system every day
echo "0 0 * * * dietpi-update" >> crontab.tmp
echo "0 0 * * * apt update && apt upgrade" >> crontab.tmp
# write temporary crontab entries to crontab
crontab < crontab.tmp

# reboot to close open root shell
reboot
