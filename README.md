<!---
SPDX-FileCopyrightText: 2023 Triss <Cyb3rVix3n@runbox.com>

SPDX-License-Identifier: MIT
--->

# Readme

[![REUSE status](https://api.reuse.software/badge/gitlab.torproject.org/Cyb3rVix3n/dietflake)](https://api.reuse.software/info/gitlab.torproject.org/Cyb3rVix3n/dietflake)

This repository only provides configuration files for DietPi. You have to download the actual DietPi-image from the DietPi webpage. If you wanna check out DietPi, have a look at https://dietpi.com

In case you need further information on Snowflake, have a look at https://snowflake.torproject.org/

You can find the list of supported and tested Devices [here.](Supported_Devices.md) It is very possible that also other divices work that are supported by DietPi.

Instructions for installation can be found [here.](INSTALL/Installation_en.md)
